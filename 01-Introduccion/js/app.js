// Instalar VS Code
// Instalar complemento LiveServe

// alert("Hola Mundo que tal?")

'use strict';

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Nuestro primer programa en JS

// const nombre = prompt('Cual es tu nombre?');
// document.querySelector('.contenido').innerHTML = ` ${nombre} está aprendiendo JavaScript`;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Comentar el código (Clean Code)

// Existen 2 tipos de comentarios, de una linea o de múltiples lineas

/*

*/

// // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// // Todos los navegadores tienen una consola de Javascript
// // La consola es muy útil ya que podrás observar tu código JS, resultados y seleccionar elementos
// // Se pueden crear variables e imprimirlas

// let hola = "hola desde la consola que tal?";
// console.log(hola);

// console.log(hola.length)


// console.log(typeof hola)
// let edad = 34;
// console.log(edad)
// console.log(edad.length)


// console.log(typeof edad)

// // // También se pueden crear arreglos

// let numeros = [1, 2, 3, 6, 5, 6, 90];

// console.log(numeros);
// console.log(typeof numeros);

// console.log(numeros.length)



console.log("VSCode: comentar una línea en  CTRL+K+C")

console.log("VSCode: Descomentar una línea en  CTRL+K+U")


// // De la misma forma se pueden crear objetos
// let obj = { nombre: "Jose", profesion: "Desarrollador Web" }
// console.log( obj );

// console.log(typeof obj)
// // También se pueden imprimir resultados como tabla
// console.table([1, 2, 3, 4]);

// // o mostrar algún error
console.error("Algo salió mal");

// // // Limpiar la consola
// console.clear();

// // O enviar advertencias
// console.warn("Eso no esta permitido");

// // También monitorear el tiempo que tarda algo en ejecutarse
console.time('Hola'); console.warn("Eso no esta permitido"); console.warn("Eso no esta permitido");
// console.warn("Eso no esta permitido");
// console.timeEnd('Hola');
   
// // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

// // Código ordenado

// // JavaScript tiene una caracteristica, a diferencia de otros lenguajes de programación el ; al final  no es obligatorio salvo que tengas 2 líneas de código en una misma.
// // ejemplo

// console.log('Hola')
// console.log('Mundo')

// // va a funcionar, pero si tienes un código desordenado; no va a funcionar salvo que pongas ; 
// console.log('Hola'); console.log('Mundo');

// // Como recomendación siempre pon una instrucción por linea, no es obligatorio salvo el caso que vimos anteriormente pero va a facilitar mucho tu código

// // ahora, vamos a crear una función, no te preocupes aun por funciones lo veremos más adelante

// // function hola() {
// // console.log('ok')
// // }

// // en estos casos esta función es valida y va a funcionar, pero es mejor utilizar tabs o espacios 


