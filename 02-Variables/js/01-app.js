// Las variables son una característica de cualquier lenguaje de programación

// existen 3 formas de crear variables en JavaScript, en nuevas versiones solo se utilizan 2:

// let era la forma de crear variables en versiones anteriores de ECMASscript hoy en día las opciones se reducen a 2

// let producto = 'Monitor 23 Pulgadas'; // Inicializamos una variable con un valor;

// // Las variables también se pueden reasignar
// producto = 'Monitor de 19 Pulgadas';

// console.log(producto);

// console.log(typeof producto);

// producto = 24

// console.log(producto);

// console.log(typeof producto);


// // Javascript es un lenguaje de tIpo Dinamico,
// // No se especifican tipos de datos cuando
// // se crea la variable
// let precio = 200;
// console.log(precio);

// // También se puede inicializar una variable sin valor y asignarlo después

// let disponible;
// disponible = true;


// // Inicializar múltiples variables 
// let categoria = 'Computadoras',
//     marca = 'Marca Famosa',
//     calificacion = 5;

// console.log(categoria)
// console.log(marca)
// console.log(calificacion)


// // Reglas de las variables:

// // Pueden tener: letras, numeros, _
// // No pueden iniciar con numero
// let 99dias;
// let dias99;

// let _01;
// let 01_;

// // Estilos para nombrar variables con más de una palabra

// // más de una palabra.

console.log("Clean Code")

// let nP = 'Monitor 30 Pulgadas';
// let nombre = 'Monitor 30 Pulgadas';
// let producto = 'Monitor 30 Pulgadas';



let nombreProducto = 'Monitor 30 Pulgadas'; // CamelCase

let nombre_producto = 'Monitor 30 Pulgadas'; // underscore

let NombreProducto = 'Monitor 30 Pulgadas'; // pascal case



// // Existen pocas diferencias entre let y const, asi que vamos a verlas:

// // primero una variable con const su valor no puede ser re-asignado:

// const producto = 'Monitor 30 Pulgadas';
// console.log(producto);
// producto = 'Monitor 23 Pulgadas';

// // Por otro lado, las variables con const, deben iniciarse con un valor:
// const precio;
// precio = 20;
// console.log(precio);

// Existen otras diferencias entre const y let especialmente cuando se trabajan con arreglos y objetos, que te mostraré mas adelante, pero recuerda, las variables con const, no se pueden re-asignar y tampoco pueden iniciar sin un valor.
